## SMD Overview

SMD files are binary files containing indexed geometry split up into submeshes called shapes.

As with many other GGG file formats, the data in the GGPK archive is compressed with Brotli. In this case, the file starts with three signature bytes `CMP` followed by a `uint32_t` containing the size of the uncompressed payload, followed by the compressed payload.

The format version described is version `1`.

Quantities are little endian and densely packed ignoring natural alignment.
Strings and string tables are not terminated and have previously defined lengths. Some strings are narrow and some strings are UTF-16. Lengths may be either in code units or bytes, even for wide strings.

The payload has:

* a bounding box;
* a list of shapes and their triangle ranges;
* a list of indices forming triangles;
* a list of vertex format structs with attributes;
* a possibly larg etrailer with unknown content, typically zero.

Indices are either 16-bit or 32-bit, determined  by the number of vertices in the data.

The vertex format is partially determined to consist of:

* XYZ position with 32-bit floats;
* eight bytes of unknown data;
* UV coordinates with 16-bit floats;
* four bone indices in 8-bit unsigned integers;
* four bone weights in 8-bit unsigned integers.

The coordinate system seems to have +Z up.

Texture coordinates appear to be allowed to be negative, assumedly to allow for mirrored sampling without UV discontinuities.

The bone indices are references to bones in the skeleton defined in AST files.

The bone weights sum up to 0xFF with unused bones having zero weight. Used bones are densely packed at the start of the list.

## File format

### Header
```cpp
uint8_t version; // 1
uint32_t triangle_count;
uint32_t vertex_count;
uint8_t const_04; // always 0x04
uint16_t shape_count;
```

### Shape list
`shape_count` instances of the `shape_info` struct, where the `name_byte_count` is used for a string table defined later and the `triangle_offset` is into the list of indices, indicating the start of the shape.

```cpp
struct shape_info {
    uint32_t name_byte_count, triangle_offset;
};
```

### Shape name list
`shape_count` instances of UTF-16 encoded strings with their lengths indicated by the corresponding `name_byte_count` holding their length in bytes. The strings are not terminated.

### Indexed triangle list
`triangle_count` instances of indexed triangles, referencing into a later list of vertex attributes. Indexes are unsigned 16-bit or 32-bit integers, determined by whether there are more than `0xFFFF` vertices.

```cpp
struct indexed_triangle {
    uintNN_t indices[3];
};

### Vertex data list
`vertex_count` instances of the vertex format.

```cpp
struct vertex_format {
    float3 position;
    uint8_t unknown[8];
    half2 uv;
    uint8_t bone_indices[4];
    uint8_t bone_weights[4];
};
```

### Trailer
A block with what appears to be a leading count of some sort, followed by a number of zero bytes somewhat related to the count.

If the trailer bytes are not zero, the content following it seems to be of floating point nature.