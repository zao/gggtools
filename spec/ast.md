## AST Overview

AST files are binary files containing skeleton bones and named light attachment points, as well as a set of animations with tracks for individual bones and attachment points.

SMD files are binary files containing indexed geometry split up into submeshes called shapes.

As with many other GGG file formats, the data in the GGPK archive is compressed with Brotli. In this case, the file starts with three signature bytes `CMP` followed by a `uint32_t` containing the size of the uncompressed payload, followed by the compressed payload.

The format version described is version `1`.

Quantities are little endian and densely packed ignoring natural alignment.
Strings not terminated and have previously defined lengths. The strings are narrow and have lengths specified in bytes.

The payload has:

* a skeleton of named bones with transforms;
* a set of named shapes/attachments;
* a set of animations, each with a set of keyframe tracks for individual bones/attachments.

Animation tracks have a bone index and up to six distinct quantities it modifies.

Three of those seem to be scale, rotation and translation.
The remaining three have unknown meaning but consist of three, four, and three components.

A keyframe has a number that seems like a frame timestamp followed by the quantity value for that keyframe.

The keyframes are ordered and sparse.

## File format

