#include "dds.h"

#include <algorithm>

std::unique_ptr<dds_data>
read_dds_data(char const* p, size_t n)
{
    auto advance = [](char const*& q, size_t& k, size_t amt) {
        q += amt;
        k -= amt;
    };
    char const* q = p;
    size_t k = n;
    if (k < 4 || memcmp(q, "DDS ", 4) != 0) {
        return {};
    }
    advance(q, k, 4);

    dds_header header = {};
    if (k < sizeof(dds_header)) {
        return {};
    }
    memcpy(&header, q, sizeof(dds_header));
    advance(q, k, sizeof(dds_header));

    dds_format format = dds_format::unhandled;
    auto& pf = header.pixel_format;
    if (pf.flags & (uint32_t)dds_pf_flags::fourcc) {
        if (memcmp(pf.fourcc, "DXT", 3) == 0) {
            switch (pf.fourcc[3]) {
                case '1':
                    format = dds_format::dxt1;
                    break;
                case '2':
                    format = dds_format::dxt2;
                    break;
                case '3':
                    format = dds_format::dxt3;
                    break;
                case '4':
                    format = dds_format::dxt4;
                    break;
                case '5':
                    format = dds_format::dxt5;
                    break;
                default:
                    break;
            }
        } else if (memcmp(pf.fourcc, "o\0\0\0", 4) == 0) {
            format = dds_format::r16f;
        } else if (memcmp(pf.fourcc, "t\0\0\0", 4) == 0) {
            format = dds_format::r32g32b32a32f;
        }
    } else {
        uint32_t const rgb_mask = (uint32_t)dds_pf_flags::rgb;
        uint32_t const rgba_mask =
          (uint32_t)dds_pf_flags::rgb | (uint32_t)dds_pf_flags::alphapixels;
        uint32_t const l_mask = (uint32_t)dds_pf_flags::luminance;
        ;
        uint32_t const la_mask = (uint32_t)dds_pf_flags::luminance |
                                 (uint32_t)dds_pf_flags::alphapixels;
        switch (pf.flags) {
            case rgb_mask:
                format = dds_format::r8g8b8;
                break;
            case rgba_mask:
                format = dds_format::r8g8b8a8;
                break;
            case l_mask:
                format = dds_format::l8;
                break;
            case la_mask:
                format = dds_format::l8a8;
                break;
            default:
                break;
        }
    }

    if (format == dds_format::unhandled) {
        return {};
    }

    auto ret = std::make_unique<dds_data>();
    ret->header = header;
    ret->format = format;
    ret->payload_data = q;
    ret->payload_size = k;
    return ret;
}

format_info_gl
derive_format_info(dds_data& info)
{
    format_info_gl ret{};
    switch (info.format) {
        case dds_format::l8:
            ret.internal_format = GL_LUMINANCE8;
            ret.format = GL_LUMINANCE;
            ret.type = GL_UNSIGNED_BYTE;
            break;
        case dds_format::l8a8:
            ret.internal_format = GL_LUMINANCE8_ALPHA8;
            ret.format = GL_LUMINANCE_ALPHA;
            ret.type = GL_UNSIGNED_BYTE;
            break;
        case dds_format::r16f:
            ret.internal_format = GL_R16F;
            ret.format = GL_RED;
            ret.type = GL_HALF_FLOAT;
            break;
        case dds_format::r32g32b32a32f:
            ret.internal_format = GL_RGBA32F;
            ret.format = GL_RGBA;
            ret.type = GL_FLOAT;
            break;
        case dds_format::r8g8b8:
            ret.internal_format = GL_RGB8;
            ret.format = GL_RGBA;
            ret.type = GL_UNSIGNED_BYTE;
            break;
        case dds_format::r8g8b8a8:
            ret.internal_format = GL_RGBA8;
            ret.format = GL_RGBA;
            ret.type = GL_UNSIGNED_BYTE;
            break;
        case dds_format::dxt1:
            ret.internal_format = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
            ret.premultiplied = true;
            goto compressed_common;
        case dds_format::dxt2:
            ret.internal_format = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
            ret.premultiplied = true;
            goto compressed_common;
        case dds_format::dxt3:
            ret.internal_format = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
            ret.premultiplied = false;
            goto compressed_common;
        case dds_format::dxt4:
            ret.internal_format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
            ret.premultiplied = true;
            goto compressed_common;
        case dds_format::dxt5:
            ret.internal_format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
            ret.premultiplied = false;
        compressed_common:
            ret.is_compressed = true;
            ret.block_size = (info.format == dds_format::dxt1) ? 8 : 16;
            break;
    }
    return ret;
}

GLuint
load_dds_from_mem_gl(char const* p, size_t n)
{
    auto info = read_dds_data(p, n);
    if (!info) {
        return 0;
    }

    GLuint old_tex;
    GLuint tex;
    glGenTextures(1, &tex);
    glGetIntegerv(GL_TEXTURE_BINDING_2D, (GLint*)&old_tex);
    glBindTexture(GL_TEXTURE_2D, tex);
    int mip_count = (std::max)(info->header.mip_map_count, 1u);
    glTexParameteri(
      GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, mip_count - 1);

    format_info_gl format_info = derive_format_info(*info);

    char const* q = info->payload_data;
    size_t width = info->header.width;
    size_t height = info->header.height;
    for (size_t mip = 0; mip < mip_count; ++mip) {

        if (format_info.is_compressed) {
            size_t level_size =
              ((width + 3) / 4) * ((height + 3) / 4) * format_info.block_size;
            glCompressedTexImage2D(GL_TEXTURE_2D,
                                   (GLsizei)mip,
                                   format_info.internal_format,
                                   (GLsizei)width,
                                   (GLsizei)height,
                                   0,
                                   (GLsizei)level_size,
                                   q);
        } else {
            glTexImage2D(GL_TEXTURE_2D,
                         (GLsizei)mip,
                         format_info.internal_format,
                         (GLsizei)width,
                         (GLsizei)height,
                         0,
                         format_info.format,
                         format_info.type,
                         q);
        }
        width = (std::max<size_t>)(width / 2, 1);
        height = (std::max<size_t>)(height / 2, 1);
    }

    glBindTexture(GL_TEXTURE_2D, old_tex);
    return tex;
}
