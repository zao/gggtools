#pragma once

#include <GL/glew.h>
#include <cstdint>
#include <memory>

enum class dds_pf_flags : uint32_t
{
    alphapixels = 0x1,
    alpha = 0x2,
    fourcc = 0x4,
    rgb = 0x40,
    yuv = 0x200,
    luminance = 0x20000,
};
struct dds_pixelformat
{
    uint32_t size;
    uint32_t flags;
    char fourcc[4];
    uint32_t rgb_bit_count;
    uint32_t r_bit_mask;
    uint32_t g_bit_mask;
    uint32_t b_bit_mask;
    uint32_t a_bit_mask;
};
enum class dds_flags : uint32_t
{
    caps = 0x1,
    height = 0x2,
    width = 0x4,
    pitch = 0x8,
    pixel_format = 0x1000,
    mipmap_count = 0x20000,
    linear_size = 0x80000,
    depth = 0x800000,
};
enum class dds_caps : uint32_t
{
    complex = 0x8,
    mipmap = 0x400000,
    texture = 0x1000,
};
struct dds_header
{
    uint32_t size;
    dds_flags flags;
    uint32_t height;
    uint32_t width;
    uint32_t pitch_or_linear_size;
    uint32_t depth;
    uint32_t mip_map_count;
    uint32_t reserved1[11];
    dds_pixelformat pixel_format;
    dds_caps caps;
    uint32_t caps2;
    uint32_t caps3;
    uint32_t caps4;
    uint32_t reserved2;
};

enum class dds_format
{
    unhandled,
    dxt1,
    dxt2,
    dxt3,
    dxt4,
    dxt5,
    l8a8,
    l8,
    r8g8b8,
    r8g8b8a8,
    r32g32b32a32f,
    r16f,
};

struct dds_data
{
    dds_header header;
    dds_format format;
    char const* payload_data;
    size_t payload_size;
};

std::unique_ptr<dds_data>
read_dds_data(char const* p, size_t n);

struct format_info_gl
{
    GLenum internal_format;
    bool premultiplied;
    bool is_compressed;
    union
    {
        struct // compressed
        {
            uint8_t block_size;
        };
        struct // uncompressed
        {
            GLenum format;
            GLenum type;
        };
    };
};

format_info_gl
derive_format_info(dds_data& info);

GLuint
load_dds_from_mem_gl(char const* p, size_t n);
