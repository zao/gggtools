#include <GL/glew.h>
#include <Windows.h>

#include <GLFW/glfw3.h>

#include "gb_math.h"
#include "stb_image_write.h"

#include <brotli/decode.h>
#include <gggtools/ggg.h>

#include <algorithm>
#include <array>
#include <atomic>
#include <cassert>
#include <deque>
#include <functional>
#include <future>
#include <map>
#include <memory>
#include <mutex>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include "dds.h"
#include "ggg_utils.h"
#include "mat.h"
#include "sm.h"
#include "smd.h"

static GLFWwindow* g_win;

static int
my_main(int argc, char** argv);

#if defined(_MSC_VER)
#include <Windows.h>

int
WinMain(HINSTANCE hinst, HINSTANCE prev_instance, LPSTR args, int show_cmd)
{
    return my_main(__argc, __argv);
}
#else
int
main(int argc, char** argv)
{
    return my_main(argc, argv);
}
#endif

static void
on_window_close(GLFWwindow* win)
{
    glfwSetWindowShouldClose(win, true);
}

std::vector<float> smd_floats[4];
std::vector<char> smd_data;

void
test_dds(ggg_ggpk* ggpk, ggg_file* file)
{
    file = resolve_symlinks(ggpk, file);
    char const* p = ggg_file_data(file);
    size_t n = ggg_file_size(file);
    std::unique_ptr<std::vector<char>> uncomp;
    char const* uncomp_p = nullptr;
    size_t uncomp_cb = 0;
    assert(n >= 4);
    if (memcmp(p, "DDS ", 4) == 0) {
        uncomp_p = p;
        uncomp_cb = n;
    } else {
        uncomp = decompress_brotli(p, n);
        uncomp_p = uncomp->data();
        uncomp_cb = uncomp->size();
    }

    {
        char buf[1024]{};
        sprintf(
          buf, "C:\\Temp\\ggg-dds\\%s", ggg_entry_leafname((ggg_entry*)file));
        HANDLE fh = CreateFileA(
          buf, GENERIC_WRITE, 0, nullptr, CREATE_ALWAYS, 0, nullptr);
        DWORD bytes_written = 0;
        WriteFile(fh, uncomp_p, (DWORD)uncomp_cb, &bytes_written, nullptr);
        CloseHandle(fh);
    }
}

void
decompress_smd_file_to_temp(ggg_file* file)
{
    char const* p = ggg_file_data(file);
    size_t n = ggg_file_size(file);
    assert(n >= 3 && memcmp(p, "CMP", 3) == 0);
    auto uncomp = decompress_brotli(p + 3, n - 3);

    if (uncomp) {
        char buf[1024]{};
        sprintf(
          buf, "C:\\Temp\\ggg-smd\\%s", ggg_entry_leafname((ggg_entry*)file));
        HANDLE fh = CreateFileA(
          buf, GENERIC_WRITE, 0, nullptr, CREATE_ALWAYS, 0, nullptr);
        DWORD bytes_written = 0;
        WriteFile(
          fh, uncomp->data(), (DWORD)uncomp->size(), &bytes_written, nullptr);
        CloseHandle(fh);
    }
}

int current_submesh = 0;

void
on_key(GLFWwindow* win, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_M && action == GLFW_RELEASE) {
        if (mods == GLFW_MOD_SHIFT) {
            current_submesh -= 1;
        } else if (mods == 0) {
            current_submesh += 1;
        }
    }
}

int
my_main(int argc, char** argv)
{
    if (argc != 2) {
        return 1;
    }

    ggg_ggpk* ggpk = ggg_ggpk_open(argv[1]);

#if 0
    {
        std::mutex gather_mutex;
        using info_name_t = std::pair<std::string, dds_data>;
        std::map<size_t, std::vector<info_name_t>> mip_counts;
        std::map<std::string, dds_data> lballs;
        visit_extension_context ctx{};
        ctx.user_data = ggpk;
        ctx.extension = ".dds";
        ctx.on_match_fun =
          [ggpk, &mip_counts, &gather_mutex, &lballs](ggg_file* file) {
              std::string full_name = entry_full_name((ggg_entry*)file);
              file = resolve_symlinks(ggpk, file);

              std::unique_ptr<std::vector<char>> data;
              char const* p = ggg_file_data(file);
              size_t n = ggg_file_size(file);
              if (n >= 4 && memcmp(p, "DDS ", 4) != 0) {
                  data = decompress_file_to_vector(p, n);
              }
              if (data) {
                  p = data->data();
                  n = data->size();
              }
              auto info = read_dds_data(p, n);
              assert(info);
              size_t mip_count = 0;
              if ((uint32_t)info->header.flags &
                  (uint32_t)dds_flags::mipmap_count) {
                  mip_count = info->header.mip_map_count;
              }
              std::unique_lock<std::mutex> lk(gather_mutex);
              mip_counts[mip_count].push_back({ full_name, *info });

#if 0
              {
                  std::string solid_name = full_name.substr(1);
                  for (auto& ch : solid_name) {
                      if (ch == '/') {
                          ch = '$';
                      }
                  }
                  solid_name = "C:\\Temp\\ggg-dds\\" + solid_name;
                  HANDLE fh = CreateFileA(solid_name.c_str(),
                                          GENERIC_WRITE,
                                          0,
                                          nullptr,
                                          CREATE_ALWAYS,
                                          0,
                                          nullptr);
                  DWORD bytes_written{};
                  WriteFile(fh, p, (DWORD)n, &bytes_written, nullptr);
                  CloseHandle(fh);
              }
#endif

              if (stricmp(ggg_entry_leafname((ggg_entry*)file),
                          "lightning_ball_white.dds") == 0) {
                  lballs[full_name] = *info;
              }
          };
        visit_files_with_extension(ggg_ggpk_root(ggpk), &ctx);
        DebugBreak();
    }

    return 0; // NOTE(zao): exits
#endif

    glfwInit();

    int hints[] = {
        GLFW_CONTEXT_VERSION_MAJOR, 4,
        GLFW_CONTEXT_VERSION_MINOR, 5,
        GLFW_OPENGL_PROFILE,        GLFW_OPENGL_COMPAT_PROFILE,
    };
    size_t hint_count = sizeof(hints) / sizeof(hints[0]) / 2;

    for (size_t i = 0; i < hint_count; ++i) {
        glfwWindowHint(hints[2 * i], hints[2 * i + 1]);
    }

    g_win = glfwCreateWindow(1024, 1024, "ggpkviewer", NULL, NULL);
    if (!g_win) {
        return 0;
    }
    glfwMakeContextCurrent(g_win);
    glewExperimental = 1;
    glewInit();

    glEnable(GL_DEPTH_TEST);

    glfwSetWindowCloseCallback(g_win, on_window_close);
    glfwSetKeyCallback(g_win, on_key);

#define ROOTDIR "Art/Models/Chests/PerandusChest/"
    char const* pc_sm = ROOTDIR "PerandusChest.sm";
    char const* pc_smd = ROOTDIR "PerandusChestModel_07dc0372.smd";
    char const* pc_ast = ROOTDIR "PerandusChest.ast";
    char const* pc_amd = ROOTDIR "PerandusChest.amd";

    pc_sm = "Art/Microtransactions/Town_Portal/Dominator_portal/model/"
            "dominator_portal.sm";
    pc_smd = "Art/Microtransactions/Town_Portal/Dominator_portal/model/"
             "dominator_portal_rig_3a879e22.smd";
    pc_ast = "Art/Microtransactions/Town_Portal/Dominator_portal/model/"
             "dominator_portal_rig.ast";
    pc_amd = "Art/Microtransactions/Town_Portal/Dominator_portal/model/"
             "dominator_portal_rig.amd";

    // pc_smd = "Art/Models/CHARACTERS/Hunter/rig_d5e3e02c.smd";

    ggg_file* sm_file = (ggg_file*)ggg_ggpk_lookup(ggpk, pc_sm);
    ggg_file* ast_file = (ggg_file*)ggg_ggpk_lookup(ggpk, pc_ast);
    ggg_file* amd_file = (ggg_file*)ggg_ggpk_lookup(ggpk, pc_amd);

    auto mesh_desc = load_sm_file(sm_file);
    assert(mesh_desc->version == 4);
    auto mesh_desc_v4 = static_cast<sm_v4*>(mesh_desc.get());
    ggg_file* smd_file =
      (ggg_file*)ggg_ggpk_lookup(ggpk, mesh_desc_v4->smd_path.c_str());
    std::unique_ptr<smd> mesh_data = load_smd_file(smd_file);
    std::vector<mat_v3> mats;
    for (auto& mat_desc : mesh_desc_v4->materials) {
        ggg_file* mat_file =
          (ggg_file*)ggg_ggpk_lookup(ggpk, mat_desc.mat_path.c_str());
        auto mat_data = load_mat_file(mat_file);
        assert(mat_data->version == 3);
        std::unique_ptr<mat_v3> mat_data_v3(
          static_cast<mat_v3*>(mat_data.release()));
        mats.push_back(*mat_data_v3);
    }

    struct gpu_material
    {
        GLuint colour_texture;
        GLuint normal_specular_texture;
    };

    std::vector<gpu_material> gpu_materials;
    for (auto& mat : mats) {
        ggg_file* actual_file =
          resolve_symlinks(ggpk,
                           (ggg_file*)ggg_ggpk_lookup(
                             ggpk, unquote(mat.colour_texture).c_str()));
        char const* p = ggg_file_data(actual_file);
        size_t n = ggg_file_size(actual_file);
        std::unique_ptr<std::vector<char>> data;
        if (n >= 4 && memcmp(p, "DDS ", 4) != 0) {
            data = decompress_brotli(p, n);
        }
        if (data) {
            p = data->data();
            n = data->size();
        }
        gpu_material out{};
        out.colour_texture = load_dds_from_mem_gl(p, n);
        gpu_materials.push_back(out);
    }

    while (1) {
        glfwPollEvents();
        if (glfwWindowShouldClose(g_win)) {
            break;
        }

        glClearColor(0.1f, 0.2f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        double now = glfwGetTime();

        glDisable(GL_DEPTH_TEST);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_BLEND);

        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        int win_w, win_h;
        glfwGetFramebufferSize(g_win, &win_w, &win_h);
        gluPerspective(90.0f, (float)win_w / (float)win_h, 0.1f, 1000.0f);
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glLoadIdentity();
        gluLookAt(0.0f, 100.0f, 300.0f, 0.0f, 50.0f, 0.0f, 0.0f, 1.0f, 0.0f);
        glRotatef((float)now * 180.0f / 3.14f, 0.0f, 1.0f, 0.0f);
        glRotatef(90.0f, 1.0f, 0.0f, 0.0f);

        glEnable(GL_CULL_FACE);
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, gpu_materials.front().colour_texture);

#if 1
        if (mesh_data) {
            glBegin(GL_TRIANGLES);
            for (size_t j = 0; j < mesh_data->submesh_starts.size(); ++j) {
                size_t from = mesh_data->submesh_starts[j];
                size_t to = mesh_data->submesh_ends[j];
                for (size_t i = from; i < to; ++i) {
                    uint32_t* idxs = &mesh_data->indices[i * 3];
                    gbVec3 normal;
                    auto emit_vertex = [&](uint32_t idx) {
                        gbVec3* p = &mesh_data->positions[idx];
                        bone_weights* bw = &mesh_data->bone_weights[idx];
                        gbVec2* tc = &mesh_data->texcoords[idx];
                        glColor4f(bw->w[0] / 255.0f,
                                  bw->w[1] / 255.0f,
                                  bw->w[2] / 255.0f,
                                  0.1f);
                        glColor3f(tc->x, tc->y, 0.0f);
                        glNormal3fv(normal.e);
                        glVertex3f(p->x, p->y, p->z);
                    };
                    {
                        gbVec3 ps[] = { mesh_data->positions[idxs[0]],
                                        mesh_data->positions[idxs[1]],
                                        mesh_data->positions[idxs[2]] };
                        gbVec3 e1, e2;
                        gb_vec3_sub(&e1, ps[1], ps[0]);
                        gb_vec3_sub(&e2, ps[2], ps[0]);
                        gb_vec3_cross(&normal, e1, e2);
                    }
                    emit_vertex(idxs[0]);
                    emit_vertex(idxs[1]);
                    emit_vertex(idxs[2]);
                }
            }
            glEnd();
        }
#endif
#if 1
        if (mesh_data) {
            glBegin(GL_POINTS);
            for (size_t i = 0; i < mesh_data->mysteries.size(); ++i) {
                auto emit_vertex = [&](uint32_t idx) {
                    gbVec3* p = &mesh_data->positions[idx];
                    bone_weights* bw = &mesh_data->bone_weights[idx];
                    glColor3f(
                      bw->w[0] / 255.0f, bw->w[1] / 255.0f, bw->w[2] / 255.0f);
                    glVertex3f(p->x, p->y, p->z);
                };
                emit_vertex((uint32_t)i);
            }
            glEnd();
        }
#endif

        glPopMatrix();
        glfwSwapBuffers(g_win);
    }

    if (g_win) {
        glfwDestroyWindow(g_win);
    }
    glfwTerminate();
    return 0;
}
