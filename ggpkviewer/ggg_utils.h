#pragma once

#include <functional>
#include <memory>
#include <string>
#include <vector>

#include <gggtools/ggg.h>

struct visit_extension_context
{
    std::string extension;
    std::function<void(ggg_file*)> on_match_fun;

    void* user_data;
};

void
visit_files_with_extension(ggg_directory* base_dir,
                           visit_extension_context* ctx);

std::unique_ptr<std::vector<char>>
decompress_brotli(char const* p, size_t n);

ggg_file*
resolve_symlinks(ggg_ggpk* ggpk, ggg_file* file);

std::string
entry_full_name(ggg_entry* e);

std::string
unquote(std::string s);

std::string
wctmb(wchar_t const* p, size_t cch);
