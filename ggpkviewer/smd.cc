#include "smd.h"

#include <Windows.h>

#include <brotli/decode.h>
#include <gggtools/ggg.h>

std::unique_ptr<smd>
load_smd_file(ggg_file* file)
{
    char const* p = ggg_file_data(file);
    size_t n = ggg_file_size(file);
    if (n < 7 || memcmp(p, "CMP", 3) != 0) {
        return {};
    }
    size_t uncomp_cb{};
    memcpy(&uncomp_cb, p + 3, 4);
    size_t comp_cb = n - 7;
    char const* comp_p = p + 7;
    std::vector<char> uncomp(uncomp_cb);
    char* uncomp_p = uncomp.data();

    BrotliDecoderState* bdec =
      BrotliDecoderCreateInstance(nullptr, nullptr, nullptr);
    BrotliDecoderResult res = BrotliDecoderDecompress(
      comp_cb, (uint8_t const*)comp_p, &uncomp_cb, (uint8_t*)uncomp_p);
    BrotliDecoderDestroyInstance(bdec);
    if (res != BROTLI_DECODER_SUCCESS) {
        return {};
    }

    char const* q = uncomp_p;

    uint8_t version;
    memcpy(&version, q, 1);
    q += 1;

    if (version != 0x01) {
        return {};
    }

    uint32_t triangle_count;
    memcpy(&triangle_count, q, 4);
    q += 4;

    uint32_t vertex_count;
    memcpy(&vertex_count, q, 4);
    q += 4;

    uint8_t unk_num1;
    memcpy(&unk_num1, q, 1);
    q += 1;

    if (unk_num1 != 0x04) {
        return {};
    }

    uint8_t shape_count;
    memcpy(&shape_count, q, 1);
    q += 1;

    uint8_t unk_num2;
    memcpy(&unk_num2, q, 1);
    q += 1;

    uint32_t unk_count;
    memcpy(&unk_count, q, 4);
    q += 4;

    float bbox[6];
    memcpy(bbox, q, 24);
    q += 24;
    gbAabb3 bounds;
    bounds.centre = gb_vec3((bbox[1] + bbox[0]) / 2.0f,
                            (bbox[3] + bbox[2]) / 2.0f,
                            (bbox[5] + bbox[4]) / 2.0f);
    bounds.half_size = gb_vec3((bbox[1] - bbox[0]) / 2.0f,
                               (bbox[3] - bbox[2]) / 2.0f,
                               (bbox[5] - bbox[4]) / 2.0f);

    struct shape_info
    {
        uint32_t name_cb, triangle_offset;
    };

    std::vector<shape_info> shape_infos(shape_count);
    for (size_t i = 0; i < shape_count; ++i) {
        memcpy(&shape_infos[i], q, 8);
        q += 8;
    }

    std::vector<std::wstring> shape_names(shape_count);
    for (size_t i = 0; i < shape_count; ++i) {
        shape_names[i] = (wchar_t const*)q;
        q += shape_infos[i].name_cb;
    }

    std::vector<uint32_t> indices(triangle_count * 3);
    if (vertex_count * 3 > 0xFFFF) {
        size_t cb = indices.size() * sizeof(uint16_t);
        memcpy(indices.data(), q, cb);
        q += cb;
    } else {
        for (size_t i = 0; i < indices.size(); ++i) {
            uint16_t idx{};
            memcpy(&idx, q, sizeof(uint16_t));
            indices[i] = (uint32_t)idx;
            q += 2;
        }
    }

    std::vector<gbVec3> positions(vertex_count);
    std::vector<mystery> mysteries(vertex_count);
    std::vector<gbVec2> texcoords(vertex_count);
    std::vector<bone_indices> bone_indices(vertex_count);
    std::vector<bone_weights> bone_weights(vertex_count);
    for (size_t i = 0; i < vertex_count; ++i) {
        memcpy(&positions[i], q, sizeof(gbVec3));
        q += sizeof(gbVec3);
        memcpy(&mysteries[i], q, 8);
        q += 8;
        gbHalf uv[2];
        memcpy(uv, q, 4);
        q += 4;
        texcoords[i] =
          gb_vec2(gb_half_to_float(uv[0]), gb_half_to_float(uv[1]));
        memcpy(&bone_indices[i], q, 4);
        q += 4;
        memcpy(&bone_weights[i], q, 4);
        q += 4;
    }

    uint32_t trail_count;
    memcpy(&trail_count, q, 4);
    q += 4;

    std::vector<uint8_t> trailer(trail_count * 8);
    memcpy(trailer.data(), q, trail_count * 8);
    q += trail_count * 8;

    smd ret{};
    ret.positions = std::move(positions);
    ret.mysteries = std::move(mysteries);
    ret.texcoords = std::move(texcoords);
    ret.bone_indices = std::move(bone_indices);
    ret.bone_weights = std::move(bone_weights);
    ret.indices = std::move(indices);
    ret.bounds = bounds;

    ret.submesh_starts.resize(shape_count);
    for (size_t i = 0; i < shape_count; ++i) {
        ret.submesh_starts[i] = shape_infos[i].triangle_offset;
    }

    ret.submesh_ends.resize(shape_count);
    for (size_t i = 0; i < shape_count - 1; ++i) {
        ret.submesh_ends[i] = ret.submesh_starts[i + 1];
    }
    ret.submesh_ends[shape_count - 1] = triangle_count;

    return std::make_unique<smd>(std::move(ret));
}
