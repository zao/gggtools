#include "sm.h"

#include "ggg_utils.h"

#include <sstream>

std::unique_ptr<sm_version>
load_sm_file(ggg_file* file)
{
    char const* p = ggg_file_data(file);
    size_t n = ggg_file_size(file);
    std::string s = wctmb((wchar_t const*)p, n / 2);

    std::istringstream iss(s.data());
    auto ret = std::make_unique<sm_v4>();
    std::string tag;
    std::string smd_path;
    size_t material_count;
    if (!(iss >> tag && tag == "version" && iss >> ret->version && iss >> tag &&
          tag == "SkinnedMeshData" && iss >> smd_path && iss >> tag &&
          tag == "Materials" && iss >> material_count)) {
        return nullptr;
    }
    ret->smd_path = unquote(smd_path);
    ret->materials.resize(material_count);
    std::string mat_path;
    for (auto& mat : ret->materials) {

        if (!(iss >> mat_path && iss >> mat.value)) {
            return nullptr;
        }
        mat.mat_path = unquote(mat_path);
    }
    return ret;
}
