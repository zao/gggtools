#pragma once

#include <cstdint>
#include <memory>
#include <string>
#include <vector>

#include <gggtools/ggg.h>

struct sm_version
{
    uint32_t version;
};

struct sm_v4 : sm_version
{
    std::string smd_path;
    struct material
    {
        std::string mat_path;
        uint32_t value;
    };

    std::vector<material> materials;
};

std::unique_ptr<sm_version>
load_sm_file(ggg_file* file);
