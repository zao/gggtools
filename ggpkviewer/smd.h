#pragma once

#include "gb_math.h"
#include <memory>
#include <vector>

#include <gggtools/ggg.h>

struct mystery
{
    uint8_t unk[8];
};

struct bone_indices
{
    uint8_t i[4];
};

struct bone_weights
{
    uint8_t w[4];
};

struct smd
{
    std::vector<uint32_t> indices;
    std::vector<gbVec3> positions;
    std::vector<mystery> mysteries;
    std::vector<gbVec2> texcoords;
    std::vector<bone_indices> bone_indices;
    std::vector<bone_weights> bone_weights;
    std::vector<uint32_t> submesh_starts;
    std::vector<uint32_t> submesh_ends;
    gbAabb3 bounds;
};

std::unique_ptr<smd>
load_smd_file(ggg_file* file);
