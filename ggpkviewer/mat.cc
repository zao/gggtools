#include "mat.h"

#include "ggg_utils.h"

#include <sstream>

blend_mode
lookup_blend_mode(std::string name)
{
    if (name == "AlphaTestWithShadow") {
        return blend_mode::alpha_test_with_shadow;
    }
    if (name == "Additive") {
        return blend_mode::additive;
    }
    return blend_mode::unknown;
}

std::unique_ptr<mat_version>
load_mat_file(ggg_file* file)
{
#if 0
    Version 3
        BlendMode AlphaTestWithShadow
        SpecularExponent 32
        SourceTextures "Textures/Misc/portals/DominatorPortal/DominatorPortal_colour.png;Textures/Misc/portals/DominatorPortal/DominatorPortal_normal.png;Textures/Misc/portals/DominatorPortal/DominatorPortal_specular.png"
        ColourTexture "Art/Textures/Misc/portals/DominatorPortal/DominatorPortal_colour.dds"
        NormalSpecularTexture "Art/Textures/Misc/portals/DominatorPortal/DominatorPortalns.dds"
        Effect "GlossySpecular" "'Art/Textures/Misc/portals/DominatorPortal/DominatorPortal_gloss.dds' "
        Effect "SpecularColor" ""
        Effect "SpecularMult" "2.275000 "
        Effect "GlowAlpha" "3.500000 1.000000 0.200000 1.773000 "
        Effect "MaskedAddTexEmissive" "'Art/Textures/Misc/portals/DominatorPortal/DominatorPortal_add.dds' 0.000000 0.000000 0.400000 1.000000 1.000000 0.000000 13.000000 240.000000 0.200000 "
        Effect "ambient" "0.100000 0.100000 0.100000 "
#endif
    char const* p = ggg_file_data(file);
    size_t n = ggg_file_size(file);
    std::string s = wctmb((wchar_t const*)p, n / 2);
    std::istringstream iss(s);
    std::string tag;
    auto ret = std::make_unique<mat_v3>();
    if (!(iss >> tag && tag == "Version" && iss >> ret->version &&
          ret->version == 3)) {
        return nullptr;
    }
    while (iss >> tag) {
        if (tag == "BlendMode") {
            std::string mode_name;
            if (!(iss >> mode_name)) {
                return {};
            }
            ret->blend_mode = lookup_blend_mode(mode_name);
        } else if (tag == "SpecularExponent") {
            if (!(iss >> ret->specular_exponent)) {
                return {};
            }
        } else if (tag == "SourceTextures") {
            if (!(iss >> ret->source_textures)) {
                return {};
            }
        } else if (tag == "ColourTexture") {
            if (!(iss >> ret->colour_texture)) {
                return {};
            }
        } else if (tag == "NormalSpecularTexture") {
            if (!(iss >> ret->normal_specular_texture)) {
                return {};
            }
        } else if (tag == "Effect") {
            std::string param_name;
            std::string param_line;
            if (!(iss >> param_name && std::getline(iss, param_line))) {
                return {};
            }
            ret->effect_params[param_name] = param_line;
        } else {
            abort();
        }
    }

    return ret;
}
