#include "ggg_utils.h"

#include <deque>
#include <future>
#include <mutex>

#include <Windows.h>
#include <brotli/decode.h>

void
visit_files_with_extension(ggg_directory* base_dir,
                           visit_extension_context* ctx)
{
    if (!base_dir || !ctx || !ctx->on_match_fun) {
        return;
    }

    struct remaining_state
    {
        std::mutex mutex;
        std::condition_variable condvar;
        std::deque<std::promise<void>> promises;
    } remaining;

    std::function<void(ggg_directory*)> visit_impl =
      [&visit_impl, ctx, &remaining](ggg_directory* base_dir) {
          for (size_t i = 0; i < ggg_directory_child_count(base_dir); ++i) {
              ggg_entry* child = ggg_directory_get_child(base_dir, i);
              if (ggg_entry_is_file(child)) {
                  char const* name = ggg_entry_leafname(child);
                  size_t name_cch = strlen(name);
                  size_t ext_cch = ctx->extension.size();
                  if (name_cch >= ext_cch &&
                      stricmp(name + name_cch - ext_cch,
                              ctx->extension.c_str()) == 0) {
                      {
                          static auto call_nullary_function =
                            [](void* param) -> DWORD {
                              auto fun = (std::function<void()>*)(param);
                              (*fun)();
                              delete fun;
                              return 0;
                          };
                          {
                              std::unique_lock<std::mutex> lk(remaining.mutex);
                              remaining.promises.emplace_back();
                              auto& prom = remaining.promises.back();
                              QueueUserWorkItem(
                                call_nullary_function,
                                new std::function<void()>([ctx, &prom, child] {
                                    ctx->on_match_fun((ggg_file*)child);
                                    prom.set_value();
                                }),
                                0);
                          }
                      }
                  }
              }
          }
          for (size_t i = 0; i < ggg_directory_child_count(base_dir); ++i) {
              ggg_entry* child = ggg_directory_get_child(base_dir, i);
              if (ggg_entry_is_directory(child)) {
                  visit_impl((ggg_directory*)child);
              }
          }
      };

    visit_impl(base_dir);

    for (auto& prom : remaining.promises) {
        prom.get_future().wait();
    }
}

ggg_file*
resolve_symlinks(ggg_ggpk* ggpk, ggg_file* file)
{
    while (1) {
        char const* p = ggg_file_data(file);
        size_t n = ggg_file_size(file);
        char buf[4]{};
        if (n < 3 || p[0] != '*') {
            break;
        }
        memcpy(buf, p + n - 3, 3);
        if (stricmp(buf, "DDS") != 0) {
            break;
        }
        ggg_entry* next =
          ggg_ggpk_lookup(ggpk, std::string(p + 1, p + n).c_str());
        if (!next || !ggg_entry_is_file(next)) {
            break;
        }
        file = (ggg_file*)next;
    }
    return file;
}

std::unique_ptr<std::vector<char>>
decompress_brotli(char const* p, size_t n)
{
    if (!p || n < 4) {
        return {};
    }

    size_t comp_cb = n - 4;
    char const* comp_p = p + 4;

    size_t uncomp_cb{};
    memcpy(&uncomp_cb, p, 4); // partial copy, high half pre-zeroed
    std::vector<char> uncomp(uncomp_cb);
    char const* uncomp_p = uncomp.data();

    BrotliDecoderState* bdec =
      BrotliDecoderCreateInstance(nullptr, nullptr, nullptr);
    BrotliDecoderResult res = BrotliDecoderDecompress(
      comp_cb, (uint8_t const*)comp_p, &uncomp_cb, (uint8_t*)uncomp_p);
    BrotliDecoderDestroyInstance(bdec);

    if (res != BROTLI_DECODER_SUCCESS) {
        return {};
    }
    return std::make_unique<std::vector<char>>(std::move(uncomp));
}

std::string
entry_full_name(ggg_entry* e)
{
    std::string name = "/" + (std::string)ggg_entry_leafname(e);
    do {
        ggg_entry* parent = (ggg_entry*)ggg_entry_parent(e);
        char const* parent_name = ggg_entry_leafname(parent);
        if (!*parent_name) {
            break;
        }
        name = "/" + (std::string)ggg_entry_leafname(parent) + name;
        e = parent;
    } while (1);
    return name;
}

std::string
unquote(std::string s)
{
    if (s.size() && s.front() == '"') {
        s = s.substr(1);
    }
    if (s.size() && s.back() == '"') {
        s = s.substr(0, s.size() - 1);
    }
    return s;
}

std::string
wctmb(wchar_t const* p, size_t cch)
{
    int cb = WideCharToMultiByte(
      CP_UTF8, 0, p, (int)cch, nullptr, 0, nullptr, nullptr);
    std::vector<char> s(cb + 1);
    WideCharToMultiByte(
      CP_UTF8, 0, p, (int)cch, s.data(), cb, nullptr, nullptr);
    return s.data();
}
