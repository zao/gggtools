#pragma once

#include <cstdint>
#include <map>
#include <memory>
#include <string>

#include <gggtools/ggg.h>

struct mat_version
{
    uint32_t version;
};

enum class blend_mode
{
    unknown = 0,
    alpha_test_with_shadow,
    additive,
};

struct mat_v3 : mat_version
{
    blend_mode blend_mode;
    float specular_exponent;
    std::string source_textures;
    std::string colour_texture;
    std::string normal_specular_texture;
    std::map<std::string, std::string> effect_params;
};

blend_mode
lookup_blend_mode(std::string name);

std::unique_ptr<mat_version>
load_mat_file(ggg_file* file);
