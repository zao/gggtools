#ifdef _MSC_VER
#include <gggtools/ggg.h>
#include <gggtools/ggg_mapped_file.h>

#include <Windows.h>

struct ggg_mapped_file
{
    HANDLE file;
    void* mapping;

    void* data;
    uint64_t size;
};

ggg_mapped_file*
ggg_mapped_file_new(char const* filename)
{
    HANDLE fh = CreateFileA(
      filename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
    if (fh == INVALID_HANDLE_VALUE) {
        return NULL;
    }
    LARGE_INTEGER fileSize;
    if (GetFileSizeEx(fh, &fileSize) && fileSize.QuadPart) {
        void* mapping = CreateFileMapping(fh, NULL, PAGE_READONLY, 0, 0, NULL);
        if (!mapping) {
            CloseHandle(fh);
            return NULL;
        }
        ggg_mapped_file* ret =
          (ggg_mapped_file*)calloc(1, sizeof(ggg_mapped_file));
        ret->file = fh;
        ret->mapping = mapping;
        ret->data = MapViewOfFile(mapping, FILE_MAP_READ, 0, 0, 0);
        ret->size = fileSize.QuadPart;
        return ret;
    }
    return NULL;
}

void
ggg_mapped_file_del(ggg_mapped_file* mf)
{
    if (mf) {
        UnmapViewOfFile(mf->data);
        CloseHandle(mf->mapping);
        CloseHandle(mf->file);
        free(mf);
    }
}

void const*
ggg_mapped_file_data(ggg_mapped_file* mf)
{
    return mf ? mf->data : NULL;
}

uint64_t
ggg_mapped_file_size(ggg_mapped_file* mf)
{
    return mf ? mf->size : 0;
}

char*
ggg_wctmb(void const* codepointData, uint32_t codepointCount)
{
    wchar_t const* srcPtr = (wchar_t const*)codepointData;
    size_t dstCB =
      1 + WideCharToMultiByte(
            CP_UTF8, 0, srcPtr, codepointCount, NULL, 0, NULL, NULL);
    char* ret = (char*)calloc(dstCB, 1);
    WideCharToMultiByte(
      CP_UTF8, 0, srcPtr, codepointCount, ret, (int)dstCB, NULL, NULL);
    return ret;
}
#endif
