#include <gggtools/ggg.h>
#include <gggtools/ggg_mapped_file.h>

#include <stdlib.h>
#include <string.h>

typedef enum ggg_entry_type {
    GGG_ENTRY_FILE,
    GGG_ENTRY_DIRECTORY,
} ggg_entry_type;

struct ggg_entry
{
    ggg_ggpk* ggpk;
    char* leaf_name;
    ggg_entryid parent;
    ggg_hashid hashid;
    ggg_entry_type type;
    char digest[32];
};

struct ggg_file
{
    ggg_entry entry;

    char const* data;
    uint32_t size;
};

struct ggg_directory
{
    ggg_entry entry;

    ggg_entryid child_base;
    size_t child_count;
};

struct ggg_ggpk
{
    ggg_mapped_file* mapping;
    ggg_entry** entries;
    size_t entry_count;
};

static void
ggg_entry_del(ggg_entry* entry)
{
    if (entry) {
        free(entry->leaf_name);
        free(entry);
    }
}

static ggg_entry*
ggg_ggpk_entry(ggg_ggpk* ggpk, ggg_entryid id)
{
    return ggpk->entries[id];
}

char const*
ggg_entry_leafname(ggg_entry* entry)
{
    return entry ? entry->leaf_name : NULL;
}

ggg_directory*
ggg_entry_parent(ggg_entry* entry)
{
    if (!entry) {
        return NULL;
    }
    ggg_entry* parent = ggg_ggpk_entry(entry->ggpk, entry->parent);
    return parent->type == GGG_ENTRY_DIRECTORY ? (ggg_directory*)parent : NULL;
}

ggg_hashid
ggg_entry_hashid(ggg_entry* entry)
{
    return entry ? entry->hashid : 0;
}

int
ggg_entry_is_directory(ggg_entry* entry)
{
    return entry ? entry->type == GGG_ENTRY_DIRECTORY : 0;
}

int
ggg_entry_is_file(ggg_entry* entry)
{
    return entry ? entry->type == GGG_ENTRY_FILE : 0;
}

char const*
ggg_entry_digest(ggg_entry* entry)
{
    return entry ? entry->digest : NULL;
}

char const*
ggg_file_data(ggg_file* file)
{
    return file ? file->data : NULL;
}

uint32_t
ggg_file_size(ggg_file* file)
{
    return file ? file->size : 0;
}

ggg_entry*
ggg_directory_get_child(ggg_directory* dir, size_t index)
{
    if (!dir || index >= ggg_directory_child_count(dir)) {
        return NULL;
    }
    ggg_entryid id = (ggg_entryid)(dir->child_base + index);
    ggg_entry* e = ggg_ggpk_entry(dir->entry.ggpk, id);
    return e;
}

size_t
ggg_directory_child_count(ggg_directory* dir)
{
    return dir ? dir->child_count : 0;
}

void
ggg_ggpk_del(ggg_ggpk* ggpk)
{
    if (ggpk) {
        for (size_t i = 0; i < ggpk->entry_count; ++i) {
            ggg_entry_del(ggpk->entries[i]);
        }
        free(ggpk->entries);
        ggg_mapped_file_del(ggpk->mapping);
    }
    free(ggpk);
}

ggg_directory*
ggg_ggpk_root(ggg_ggpk* ggpk)
{
    return (ggpk && ggpk->entry_count) ? (ggg_directory*)ggg_ggpk_entry(ggpk, 0)
                                       : NULL;
}

ggg_entry*
ggg_ggpk_lookup(ggg_ggpk* ggpk, char const* path)
{
    if (!ggpk || !path) {
        return NULL;
    }
    char** components = NULL;
    size_t component_count = 0;
    {
        char const* p = path;
        while (1) {
            char const* q = strchr(p, '/');
            char* seg = NULL;
            size_t seg_len = 0;
            if (q) {
                seg_len = q - p;
            } else {
                seg_len = strlen(p);
            }
            if (seg_len) {
                seg = (char*)calloc(seg_len + 1, 1);
                memcpy(seg, p, seg_len);
                components = (char**)realloc(
                  components, (component_count + 1) * sizeof(char*));
                components[component_count] = seg;
                component_count++;
            }
            if (!q) {
                break;
            }
            p = q + 1;
        }
    }
    ggg_entry* entry = (ggg_entry*)ggg_ggpk_root(ggpk);
    for (size_t i = 0; i < component_count; ++i) {
        char const* component = components[i];
        if (entry->type != GGG_ENTRY_DIRECTORY) {
            goto cleanup;
        }
        ggg_directory* dir = (ggg_directory*)entry;
        ggg_entry* next_entry = NULL;
        for (size_t j = 0; j < ggg_directory_child_count(dir); ++j) {
            ggg_entry* current_entry = ggg_directory_get_child(dir, j);
            if (strcmp(current_entry->leaf_name, component) == 0) {
                next_entry = current_entry;
                break;
            }
        }
        if (next_entry) {
            entry = next_entry;
            continue;
        }
        entry = NULL;
    }
cleanup:
    for (size_t j = 0; j < component_count; ++j) {
        free(components[j]);
    }
    free(components);
    return entry;
}

void
ggg_directory_visit_children(ggg_directory* parent,
                             ggg_visit_directory_callback on_dir,
                             ggg_visit_file_callback on_file)
{
    for (size_t i = 0; i < ggg_directory_child_count(parent); ++i) {
        ggg_entry* child = ggg_directory_get_child(parent, i);
        switch (child->type) {
            case GGG_ENTRY_DIRECTORY:
                on_dir((ggg_directory*)child);
                break;
            case GGG_ENTRY_FILE:
                on_file((ggg_file*)child);
                break;
        }
    }
}

typedef struct ggg_ggpk_record_header
{
    uint32_t reclen;
    char tag[4];
} ggg_ggpk_record_header;

typedef struct ggg_ggpk_archive_header
{
    ggg_ggpk_record_header rec;
    uint32_t child_count;
} ggg_ggpk_archive_header;

typedef struct ggg_ggpk_free_header
{
    ggg_ggpk_record_header rec;
    uint64_t next;
} ggg_ggpk_free_header;

typedef struct ggg_ggpk_file_header
{
    ggg_ggpk_record_header rec;
    uint32_t name_len;
    uint8_t digest[32];
} ggg_ggpk_file_header;

typedef struct ggg_ggpk_pdir_header
{
    ggg_ggpk_record_header rec;
    uint32_t name_len;
    uint32_t child_count;
    uint8_t digest[32];
} ggg_ggpk_pdir_header;

typedef struct ggg_ggpk_pdir_child
{
    uint32_t leaf_hash;
    uint64_t offset;
} ggg_ggpk_child;

typedef struct ggg_span
{
    char const* p;
    size_t n;
} ggg_span;

static int
ggg_span_constrain(ggg_span* span, size_t size)
{
    if (span->n < size) {
        return 0;
    }
    span->n = size;
    return 1;
}

#define GGG_SPAN_READ_IMPL(Type)                                               \
    static int ggg_span_read_##Type(Type* out, ggg_span* span)                 \
    {                                                                          \
        size_t const cb = sizeof(Type);                                        \
        if (span->n < cb) {                                                    \
            return 0;                                                          \
        }                                                                      \
        memcpy(out, span->p, cb);                                              \
        span->p += cb;                                                         \
        span->n -= cb;                                                         \
        return 1;                                                              \
    }

#define GGG_SPAN_READ_MANY_IMPL(Type)                                          \
    static int ggg_span_read_many_##Type(                                      \
      Type* out, size_t count, ggg_span* span)                                 \
    {                                                                          \
        size_t const cb = count * sizeof(Type);                                \
        if (span->n < cb) {                                                    \
            return 0;                                                          \
        }                                                                      \
        memcpy(out, span->p, cb);                                              \
        span->p += cb;                                                         \
        span->n -= cb;                                                         \
        return 1;                                                              \
    }

GGG_SPAN_READ_IMPL(uint32_t)
GGG_SPAN_READ_IMPL(uint64_t)

GGG_SPAN_READ_MANY_IMPL(char)
GGG_SPAN_READ_MANY_IMPL(uint16_t)
GGG_SPAN_READ_MANY_IMPL(uint32_t)
GGG_SPAN_READ_MANY_IMPL(uint64_t)

static int
ggg_span_read_record_header(ggg_ggpk_record_header* rec, ggg_span* span)
{
    size_t const cb = sizeof(*rec);
    if (span->n < cb) {
        return 0;
    }
    memcpy(rec, span->p, cb);
    span->p += cb;
    span->n -= cb;
    if (rec->reclen < cb) {
        return 0;
    }
    return ggg_span_constrain(span, rec->reclen - cb);
}

static int
read_entry(ggg_ggpk* ggpk,
           ggg_entryid parentid,
           ggg_entryid selfid,
           ggg_hashid hashid,
           uint64_t offset)
{
    ggg_entry* new_entry = NULL;
    if (offset >= ggg_mapped_file_size(ggpk->mapping)) {
        goto failure;
    }
    ggg_span tail = { (char const*)ggg_mapped_file_data(ggpk->mapping) + offset,
                      ggg_mapped_file_size(ggpk->mapping) - offset };

    ggg_ggpk_record_header rec;
    if (!ggg_span_read_record_header(&rec, &tail)) {
        goto failure;
    }

    if (memcmp(rec.tag, "PDIR", 4) == 0) {
        uint32_t namelen;
        uint32_t child_count;
        uint8_t digest[32];
        if (!ggg_span_read_uint32_t(&namelen, &tail) ||
            !ggg_span_read_uint32_t(&child_count, &tail) ||
            !ggg_span_read_many_char(digest, 32, &tail)) {
            goto failure;
        }
        uint16_t* wide_name = calloc(namelen, sizeof(uint16_t));
        if (!ggg_span_read_many_uint16_t(wide_name, namelen, &tail)) {
            free(wide_name);
            goto failure;
        }
        if (tail.n != child_count * 12) {
            free(wide_name);
            goto failure;
        }
        ggg_directory* new_dir = calloc(1, sizeof(ggg_directory));
        new_dir->entry.ggpk = ggpk;
        new_dir->entry.hashid = hashid;
        new_dir->entry.leaf_name = ggg_wctmb(wide_name, namelen - 1);
        free(wide_name);
        wide_name = NULL;
        new_dir->entry.parent = parentid;
        new_dir->entry.type = GGG_ENTRY_DIRECTORY;
        memcpy(new_dir->entry.digest, digest, 32);
        new_dir->child_base = (ggg_entryid)ggpk->entry_count;
        new_dir->child_count = child_count;
        ggpk->entries[selfid] = (ggg_entry*)new_dir;
        ggpk->entries =
          realloc(ggpk->entries,
                  (ggpk->entry_count + child_count) * sizeof(ggg_entry*));
        ggpk->entry_count += child_count;
        for (size_t i = 0; i < child_count; ++i) {
            uint32_t child_hashid;
            uint64_t child_offset;
            if (!ggg_span_read_uint32_t(&child_hashid, &tail) ||
                !ggg_span_read_uint64_t(&child_offset, &tail)) {
                ggpk->entries[new_dir->child_base + i] = NULL;
                continue;
            }
            read_entry(ggpk,
                       selfid,
                       (ggg_entryid)(new_dir->child_base + i),
                       child_hashid,
                       child_offset);
        }
    } else if (memcmp(rec.tag, "FILE", 4) == 0) {
        uint32_t namelen;
        char digest[32];
        if (!ggg_span_read_uint32_t(&namelen, &tail) ||
            !ggg_span_read_many_char(digest, 32, &tail)) {
            goto failure;
        }
        uint16_t* wide_name = calloc(namelen, sizeof(uint16_t));
        if (!ggg_span_read_many_uint16_t(wide_name, namelen, &tail)) {
            free(wide_name);
            goto failure;
        }
        ggg_file* new_file = calloc(1, sizeof(ggg_file));
        new_file->entry.ggpk = ggpk;
        new_file->entry.hashid = hashid;
        new_file->entry.leaf_name = ggg_wctmb(wide_name, namelen - 1);
        free(wide_name);
        wide_name = NULL;
        new_file->entry.parent = parentid;
        new_file->entry.type = GGG_ENTRY_FILE;
        memcpy(new_file->entry.digest, digest, 32);
        new_file->data = tail.p;
        new_file->size = (uint32_t)tail.n;
        ggpk->entries[selfid] = (ggg_entry*)new_file;
    }

    return 1;

failure:
    ggg_entry_del(new_entry);
    return 0;
}

ggg_ggpk*
ggg_ggpk_open(char const* filename)
{
    ggg_mapped_file* map = ggg_mapped_file_new(filename);
    uint64_t* root_block_offsets = NULL;
    ggg_ggpk* ret = NULL;

    if (!map) {
        goto failure;
    }

    ggg_span all_mem = { ggg_mapped_file_data(map), ggg_mapped_file_size(map) };

    ggg_ggpk_record_header rec;
    ggg_span archive_span = all_mem;
    if (!ggg_span_read_record_header(&rec, &archive_span)) {
        goto failure;
    }

    uint32_t child_count;
    if (!ggg_span_read_uint32_t(&child_count, &archive_span)) {
        goto failure;
    }

    root_block_offsets = calloc(child_count, sizeof(uint64_t));
    if (!ggg_span_read_many_uint64_t(
          root_block_offsets, child_count, &archive_span)) {
        goto failure;
    }

    ret = calloc(1, sizeof(ggg_ggpk));
    ret->mapping = map;
    ret->entries = calloc(1, sizeof(ggg_entry*));
    ret->entry_count = 1;
    map = NULL;

    for (size_t i = 0; i < child_count; ++i) {
        if (!read_entry(ret, 0, 0, 0, root_block_offsets[i])) {
            goto failure;
        }
    }

    return ret;

failure:
    ggg_ggpk_del(ret);
    free(root_block_offsets);
    ggg_mapped_file_del(map);
    return NULL;
}
