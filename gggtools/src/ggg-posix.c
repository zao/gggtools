#if !defined(_MSC_VER)
#include "ggg.h"

#include <fcntl.h>
#include <iconv.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

struct ggg_mapped_file
{
    int fd;

    void* data;
    uint64_t size;
};

ggg_mapped_file*
ggg_mapped_file_new(char const* filename)
{
    int fd = open(filename, O_RDONLY);
    if (fd == -1) {
        return NULL;
    }
    struct stat st;
    fstat(fd, &st);
    size_t size = st.st_size;
    void* data = mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0u);
    if (data == (void*)-1) {
        close(fd);
        return NULL;
    }
    ggg_mapped_file* ret = calloc(1, sizeof(ggg_mapped_file));
    ret->fd = fd;
    ret->data = data;
    ret->size = size;
    return ret;
}

void
ggg_mapped_file_del(ggg_mapped_file* mf)
{
    if (mf) {
        close(mf->fd);
        free(mf);
    }
}

void const*
ggg_mapped_file_data(ggg_mapped_file* mf)
{
    return mf->data;
}

uint64_t
ggg_mapped_file_size(ggg_mapped_file* mf)
{
    return mf->size;
}

char*
ggg_wctmb(void const* codepointData, uint32_t codepointCount)
{
    size_t dstSize = codepointCount * 6 + 1;
    char* dst = (char*)calloc(dstSize, 1);
    char* srcPtr = (char*)codepointData;
    char* dstPtr = dst;
    size_t srcCB = codepointCount * 2;
    size_t dstCB = dstSize;
    iconv_t cd = iconv_open("UTF-8", "UTF-16LE");
    while (srcCB > 0) {
        iconv(cd, &srcPtr, &srcCB, &dstPtr, &dstCB);
    }
    iconv_close(cd);
    char* ret = strdup(dst);
    free(dst);
    return ret;
}
#endif
