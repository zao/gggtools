#pragma once

#if defined(__cplusplus)
extern "C" {
#endif

typedef struct ggg_mapped_file ggg_mapped_file;

ggg_mapped_file*
ggg_mapped_file_new(char const* filename);
void
ggg_mapped_file_del(ggg_mapped_file* mf);

void const*
ggg_mapped_file_data(ggg_mapped_file* mf);
uint64_t
ggg_mapped_file_size(ggg_mapped_file* mf);

char*
ggg_wctmb(void const* codepointData, uint32_t codepointCount);

#if defined(__cplusplus)
}
#endif
