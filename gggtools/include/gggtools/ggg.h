#pragma once

#include <stddef.h>
#include <stdint.h>

#if defined(__cplusplus)
extern "C" {
#endif

typedef uint32_t ggg_hashid;
typedef uint32_t ggg_entryid;

typedef struct ggg_entry ggg_entry;
typedef struct ggg_directory ggg_directory;
typedef struct ggg_file ggg_file;
typedef struct ggg_ggpk ggg_ggpk;

char const*
ggg_entry_leafname(ggg_entry* entry);
ggg_directory*
ggg_entry_parent(ggg_entry* entry);
ggg_hashid
ggg_entry_hashid(ggg_entry* entry);
int
ggg_entry_is_directory(ggg_entry* entry);
int
ggg_entry_is_file(ggg_entry* entry);
char const*
ggg_entry_digest(ggg_entry* dir);

char const*
ggg_file_data(ggg_file* file);
uint32_t
ggg_file_size(ggg_file* file);

ggg_entry*
ggg_directory_get_child(ggg_directory* dir, size_t index);
size_t
ggg_directory_child_count(ggg_directory* dir);

ggg_ggpk*
ggg_ggpk_open(char const* filename);
void
ggg_ggpk_del(ggg_ggpk* ggpk);

ggg_directory*
ggg_ggpk_root(ggg_ggpk* ggpk);

ggg_entry*
ggg_ggpk_lookup(ggg_ggpk* ggpk, char const* path);

typedef void (*ggg_visit_directory_callback)(ggg_directory*);
typedef void (*ggg_visit_file_callback)(ggg_file*);
void
ggg_directory_visit_children(ggg_directory* parent,
                             ggg_visit_directory_callback on_dir,
                             ggg_visit_file_callback on_file);

#if defined(__cplusplus)
}
#endif
