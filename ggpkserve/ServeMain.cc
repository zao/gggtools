#define GGPKSERVE_VERSION_STRING "1.2"
#define GGPKSERVE_MAJOR_VERSION 1
#define GGPKSERVE_MINOR_VERSION 2

#include <algorithm>
#include <atomic>
// #include <deque>
// #include <functional>
// #include <iostream>
// #include <regex>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

#include <brotli/decode.h>
#include <mongoose.h>
#include <signal.h>
#include <string.h>
#if defined(__linux__) || defined(__FreeBSD__)
static int
stricmp(char const* a, char const* b)
{
    return strcasecmp(a, b);
}
#endif

#include <gggtools/ggg.h>

static std::shared_ptr<ggg_ggpk> ggpk;

struct FileInfo
{
    enum class Kind
    {
        Dds,
        Other,
    };

    Kind kind;
    std::string mimeType;
};

bool
HasExtension(std::string path)
{
    size_t dotIdx = path.find_last_of('.');
    return dotIdx != std::string::npos && dotIdx != path.size() - 1;
}

std::string
Extension(std::string path)
{
    size_t dotIdx = path.find_last_of('.');
    if (dotIdx == std::string::npos || dotIdx == path.size() - 1) {
        return {};
    }
    return path.substr(dotIdx);
}

FileInfo
GetFileInfo(ggg_file* file)
{
    char const* leafName = ggg_entry_leafname((ggg_entry*)file);
    if (HasExtension(leafName)) {
        auto ext = Extension(leafName);
        if (stricmp(ext.c_str(), ".DDS") == 0) {
            return { FileInfo::Kind::Dds, "image/vnd-ms.dds" };
        }
    }
    return { FileInfo::Kind::Other, "application/octet-stream" };
}

int
ConnectionHandler(mg_connection* conn, mg_event ev)
{
    switch (ev) {
        case MG_AUTH:
            return MG_TRUE;
        case MG_REQUEST: {
            bool onlyHeaders = strcmp(conn->request_method, "HEAD") == 0;
            if (strncmp(conn->uri, "/tree", 5) == 0 && conn->uri[5] == '\0') {
                mg_send_status(conn, 302);
                mg_send_header(conn, "Location", "/tree/");
                return MG_TRUE;
            }
            if (strncmp(conn->uri, "/tree", 5) == 0 && conn->uri[5] == '/') {
                char const* uri = conn->uri + 5;
                std::ostringstream outSS;
                auto node = ggg_ggpk_lookup(ggpk.get(), uri);
                if (node == nullptr) {
                    return MG_FALSE;
                }

                if (ggg_entry_is_file(node)) {
                    ggg_file* file = (ggg_file*)node;
                    auto info = GetFileInfo(file);
                    char const* mime = mg_get_mime_type(
                      ggg_entry_leafname(node), info.mimeType.c_str());
                    char const* outData = nullptr;
                    size_t outSize = 0;
                    if (onlyHeaders) {
                        mg_send_status(conn, 200);
                        mg_send_header(conn, "Content-Type", mime);
                        return MG_TRUE;
                    }
                    std::unique_ptr<char[]> auxStorage;
                    std::string name = ggg_entry_leafname(node);
                    if (HasExtension(name) &&
                        stricmp(Extension(name).c_str(), ".dds") == 0) {
                        while (true) {
                            auto p = ggg_file_data(file);
                            auto n = ggg_file_size(file);
                            if (!n || *p != '*') {
                                break;
                            }
                            std::string child_filename{ p + 1, p + n };
                            ggg_entry* e = ggg_ggpk_lookup(
                              ggpk.get(), child_filename.c_str());
                            if (!e || !ggg_entry_is_file(e)) {
                                break;
                            }
                            file = (ggg_file*)e;
                            node = e;
                        }
                        uint32_t uncompressedSize;
                        auto fileData = ggg_file_data(file);
                        auto fileSize = ggg_file_size(file);
                        memcpy(&uncompressedSize, fileData, 4);
                        BrotliDecoderState* bdec = BrotliDecoderCreateInstance(
                          nullptr, nullptr, nullptr);
                        size_t numBytes = uncompressedSize;
                        auxStorage = std::make_unique<char[]>(numBytes);
                        BrotliDecoderResult bdres =
                          BrotliDecoderDecompress(fileSize - 4,
                                                  (uint8_t const*)fileData + 4,
                                                  &numBytes,
                                                  (uint8_t*)auxStorage.get());
                        if (bdres == BROTLI_DECODER_RESULT_SUCCESS) {
                            outData = auxStorage.get();
                            outSize = uncompressedSize;
                        } else {
                            outData = fileData;
                            outSize = fileSize;
                        }
                        BrotliDecoderDestroyInstance(bdec);
                    } else {
                        outData = ggg_file_data(file);
                        outSize = ggg_file_size(file);
                    }

                    if (outData) {
                        mg_send_status(conn, 200);
                        mg_send_header(conn, "Content-Type", mime);
                        mg_send_data(conn, outData, (int)outSize);
                    } else {
                        mg_send_status(conn, 500);
                    }
                } else if (ggg_entry_is_directory(node)) {
                    ggg_directory* dir = (ggg_directory*)node;
                    mg_send_status(conn, 200);
                    mg_send_header(conn, "Content-Type", "text/html");
                    if (onlyHeaders) {
                        return MG_TRUE;
                    }
                    mg_printf_data(conn, "<html><head></head><body>");
                    mg_printf_data(conn, "<h1>Index of %s</h1>\n", conn->uri);
                    mg_printf_data(conn, "<pre>\n");
                    std::string leafName = ggg_entry_leafname(node);
                    bool isRoot = leafName.empty();

                    auto RenderAbsolutePath =
                      [conn](std::string leaf) -> std::string {
                        std::ostringstream oss;
                        oss << conn->uri;
                        oss << leaf;
                        return oss.str();
                    };

                    auto fullName = RenderAbsolutePath(leafName);
                    char const* date = "30-Aug-2015 04:20";
                    if (!isRoot) {
                        std::string parent = conn->uri;
                        parent = parent.substr(
                          0, parent.find_last_of('/', parent.size() - 2));
                        mg_printf_data(conn,
                                       "<a href=\"%s/\">../</a> %s -\n",
                                       parent.c_str(),
                                       date);
                    }
                    std::vector<ggg_directory*> dirs;
                    std::vector<ggg_file*> files;
                    for (size_t i = 0; i < ggg_directory_child_count(dir);
                         ++i) {
                        ggg_entry* e = ggg_directory_get_child(dir, i);
                        if (ggg_entry_is_directory(e)) {
                            dirs.push_back((ggg_directory*)e);
                        } else if (ggg_entry_is_file(e)) {
                            files.push_back((ggg_file*)e);
                        }
                    }
                    for (auto& child : dirs) {
                        ggg_entry* e = (ggg_entry*)child;
                        auto leafName = ggg_entry_leafname(e);
                        mg_printf_data(
                          conn,
                          "<a href=\"%s%s/\">%s/</a> %s - [0x%08x]\n",
                          conn->uri,
                          leafName,
                          leafName,
                          date,
                          ggg_entry_hashid(e));
                    }
                    for (auto& child : files) {
                        ggg_entry* e = (ggg_entry*)child;
                        auto leafName = ggg_entry_leafname(e);
                        mg_printf_data(
                          conn,
                          "<a href=\"%s%s\">%s</a> %s %u [0x%08x]\n",
                          conn->uri,
                          leafName,
                          leafName,
                          date,
                          ggg_file_size(child),
                          ggg_entry_hashid(e));
                    }
                    mg_printf_data(conn, "</pre>\n");
                    mg_printf_data(conn, "</body></html>\n");
                }

                return MG_TRUE;
            }
            return MG_FALSE;
        }
        default:
            return MG_FALSE;
    }
}

std::atomic<bool> keepRunning;

void
InterruptSignal(int)
{
    keepRunning = false;
}

void
SetupSignals()
{
#if !defined(_MSC_VER)
    struct sigaction sa = {};
    sa.sa_handler = &InterruptSignal;
    sigaction(SIGINT, &sa, nullptr);
#endif
}

int
main(int argc, char** argv)
{
    if (argc != 2)
        return 1;
    SetupSignals();

    try {
        ::ggpk.reset(ggg_ggpk_open(argv[1]), &ggg_ggpk_del);
    } catch (std::exception&) {
        throw;
    }
    fprintf(stderr, "ggpkserve %s\n", GGPKSERVE_VERSION_STRING);
    fprintf(stderr, "Loaded, listening...\n");
    auto* srv = mg_create_server(nullptr, &ConnectionHandler);
    mg_set_option(srv, "listening_port", "8080");
    keepRunning = true;
    while (keepRunning) {
        mg_poll_server(srv, 1000);
    }
    mg_destroy_server(&srv);
    fprintf(stderr, "Ta-ta!\n");
}
